# hackerain.me

This is the hexo project for hackerain.me, which is a static blog site hosted on github. This repo contains the source files, including post, draft, asserts, etc.

## Setup

#### 1. Install git
```
sudo apt-get install git-core
or
sudo yum install git-core
```

#### 2. Install nodejs
```
wget -qO- https://raw.github.com/creationix/nvm/master/install.sh | sh
nvm install stable
```

#### 3. Install hexo
```
npm install -g hexo-cli
```

#### 4. Add travis to do auto hexo deploy

When push a commit to the repopo, will trigger travis to do auto hexo deploy to *.github.io repo.

##### 4.1. Generate a ssh key that can read/write your github repo

##### 4.2. Encrypt the ssh key file

```
$ gem install travis  # install ruby travis
$ travis login
$ travis encrypt-file ~/.ssh/id_rsa --add  # this will generate an encrypted ssh key file, named id_rsa.enc
```

##### 4.3. Create .travis.yml
```
language: node_js
before_install:
- openssl aes-256-cbc -K $encrypted_4bd0e53aefcd_key -iv $encrypted_4bd0e53aefcd_iv -in id_rsa.enc -out id_rsa -d
- mv id_rsa ~/.ssh/id_rsa
- chmod 600 ~/.ssh/id_rsa
- eval $(ssh-agent -s)
- ssh-add ~/.ssh/id_rsa
after_success:
- git config --global user.email "yugsuo@gmail.com"
- git config --global user.name "hackerain"
- hexo generate --deploy
```
